package com.smtgames.networking;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.swing.JOptionPane;

import main.Engine;

public class Networking {
	
	private String ip = "127.0.0.1";
	private int port = 8080;
	
	private Socket socket;
	private OutputStream out;
	private InputStream in;
	
	public void update(float leftRatio, float rightRatio) {
		try {
			out.write(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putFloat(rightRatio).array());
			out.write(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putFloat(leftRatio).array());
			out.flush();
			
			System.out.println("Right: " + rightRatio + " Left: " + leftRatio);
			
		} catch (IOException e) {
			e.printStackTrace();
			shutdown();
			
			JOptionPane.showMessageDialog(null, "Network error!");
		}
	}
	
	public Networking() {
		String ipInput = JOptionPane.showInputDialog("Ip: (Default - " + ip + ")");
		String portInput = JOptionPane.showInputDialog("Port: (Default - " + port + ")");
		if (ipInput != null) if (!ipInput.isEmpty()) ip = ipInput;
		if (portInput != null) if (!portInput.isEmpty()) port = Integer.parseInt(portInput);
		
		JOptionPane.showMessageDialog(null, "Connecting to " + ip + ":" + port);
		
		try {
			socket = new Socket(ip, port);
			out = socket.getOutputStream();
			in = socket.getInputStream();
			
			JOptionPane.showMessageDialog(null, "Connected!");
			
		} catch (IOException e) {
			e.printStackTrace();
			shutdown();
			
			JOptionPane.showMessageDialog(null, "Failed to connect!");
		}
	}
	
	public void shutdown() {
		try {
			if (out != null)
				socket.shutdownOutput();
			if (in != null)
				socket.shutdownInput();
			if (socket != null)
				socket.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Engine.useNetworking = false;
	}
}
