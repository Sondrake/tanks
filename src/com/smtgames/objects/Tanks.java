package com.smtgames.objects;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;

import com.smtgames.gui.VerticleSlider;
import com.smtgames.manager.KeyManager;
import com.smtgames.networking.Networking;

import gfx.ImageLoader;
import main.Engine;

public class Tanks {
	
	public static float leftRatio = 0.0f, rightRatio = 0.0f, oldTurn, oldSpeed;
	public float speed = 0.05f;
	
	private String direction;
	
	private VerticleSlider leftSlider, rightSlider;
	private Networking networking;
	private Font f;
	
	private BufferedImage background;
	
	public Tanks() {
		f = new Font("Arial", Font.BOLD, 20);
		background = new ImageLoader().load("/background.png");
		
		leftSlider = new VerticleSlider();
		rightSlider = new VerticleSlider();
		
		if (Engine.useNetworking)
			networking = new Networking();
	}
	
	private float goTowards(float f, float end) {
		if (f < end) f+=speed;
		else if (f > end) f-=speed;
		return f;
	}
	
	public void controls() {
		if (KeyManager.key[KeyEvent.VK_W] || KeyManager.key[KeyEvent.VK_UP]) {
			if (KeyManager.key[KeyEvent.VK_A] || KeyManager.key[KeyEvent.VK_LEFT]) {
				leftRatio = goTowards(leftRatio, 0.5f);
				rightRatio = goTowards(rightRatio, 1.0f);
				direction = "Forward-Left";
			} else if (KeyManager.key[KeyEvent.VK_D] || KeyManager.key[KeyEvent.VK_RIGHT]) {
				leftRatio = goTowards(leftRatio, 1.0f);
				rightRatio = goTowards(rightRatio, 0.5f);
				direction = "Forward-Right";
			} else {
				leftRatio = goTowards(leftRatio, 1.0f);
				rightRatio = goTowards(rightRatio, 1.0f);
				direction = "Forward";
			}
		} else if (KeyManager.key[KeyEvent.VK_S] || KeyManager.key[KeyEvent.VK_DOWN]) {
			if (KeyManager.key[KeyEvent.VK_A] || KeyManager.key[KeyEvent.VK_LEFT]) {
				leftRatio = goTowards(leftRatio, -0.5f);
				rightRatio = goTowards(rightRatio, -1.0f);
				direction = "Backward-Left";
			} else if (KeyManager.key[KeyEvent.VK_D] || KeyManager.key[KeyEvent.VK_RIGHT]) {
				leftRatio = goTowards(leftRatio, -1.0f);
				rightRatio = goTowards(rightRatio, -0.5f);
				direction = "Backward-Right";
			} else {
				leftRatio = goTowards(leftRatio, -1.0f);
				rightRatio = goTowards(rightRatio, -1.0f);
				direction = "Backward";
			}
		} else if (KeyManager.key[KeyEvent.VK_A] || KeyManager.key[KeyEvent.VK_LEFT]) {
			leftRatio = goTowards(leftRatio, -1.0f);
			rightRatio = goTowards(rightRatio, 1.0f);
			direction = "Left";
		} else if (KeyManager.key[KeyEvent.VK_D] || KeyManager.key[KeyEvent.VK_RIGHT]) {
			leftRatio = goTowards(leftRatio, 1.0f);
			rightRatio = goTowards(rightRatio, -1.0f);
			direction = "Right";
		} else if (!KeyManager.key[KeyEvent.VK_SHIFT]){
			leftRatio = goTowards(leftRatio, 0.0f);
			rightRatio = goTowards(rightRatio, 0.0f);
			direction = "Nothing";
		} else direction = "Freeze";
	}
	
	public void update() {
		controls();
		
		leftRatio = new BigDecimal(Float.toString(leftRatio)).setScale(2, BigDecimal.ROUND_HALF_DOWN).floatValue();
		rightRatio = new BigDecimal(Float.toString(rightRatio)).setScale(2, BigDecimal.ROUND_HALF_DOWN).floatValue();
		
		if (Engine.useNetworking) {
			if (oldTurn != leftRatio || oldSpeed != rightRatio) {
				networking.update(leftRatio, rightRatio);
				oldTurn = leftRatio;
				oldSpeed = rightRatio;
			}
		}
	}
	
	public void draw(Graphics g) {
		g.setFont(f);
		
		g.setColor(new Color(30, 30, 30));
		g.fillRect(0,0, Engine.WIDTH, Engine.HEIGHT);
		
		g.drawImage(background, 0, 0, Engine.WIDTH, Engine.HEIGHT, null);
		
		leftSlider.draw(g, Engine.WIDTH/2-70, (int)(Engine.HEIGHT/2), 60, 200, leftRatio);
		rightSlider.draw(g, Engine.WIDTH/2+10, (int)(Engine.HEIGHT/2), 60, 200, rightRatio);
		
		g.drawString("Direction:", 10, 30);
		g.drawString(direction, 10, 60);
		
		g.drawString("Width:  " + Engine.WIDTH, 10, 90);
		g.drawString("Height: " + Engine.HEIGHT, 10, 120);
		
		g.drawLine(0, Engine.HEIGHT/2, Engine.WIDTH, Engine.HEIGHT/2);
		g.drawLine(0, Engine.HEIGHT/2-200, Engine.WIDTH, Engine.HEIGHT/2-200);
		g.drawLine(0, Engine.HEIGHT/2+200, Engine.WIDTH, Engine.HEIGHT/2+200);
		g.drawLine(Engine.WIDTH/2, 0, Engine.WIDTH/2, Engine.HEIGHT);
	}
}