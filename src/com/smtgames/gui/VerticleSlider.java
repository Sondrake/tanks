package com.smtgames.gui;

import java.awt.Color;
import java.awt.Graphics;

public class VerticleSlider {
	
	public void draw(Graphics g, int x, int y, int width, int height, float ratio) {
		//GREY
		g.setColor(new Color(40, 100, 40));
		if (ratio < 0) g.setColor(new Color(140, 50, 50));
		
		//VERTICLE Slider
		g.fillRect(x, y, width, (int)(height*-ratio));
		
		//WHITE
		g.setColor(Color.WHITE);
		g.drawString((int)(100*ratio) + "%", x, y+height+20);
	}
}
