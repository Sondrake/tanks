package main;


import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import com.smtgames.manager.KeyManager;
import com.smtgames.objects.Tanks;

import gfx.ImageLoader;

public class Engine extends Canvas implements Runnable {
	private static final long serialVersionUID = 1L;
	
	public static int WIDTH = 3840/4, HEIGHT = 2160/4;
	public static boolean useNetworking = true;
	
	private static final String title = "Tank Controller V1.0";
	private Thread mainThread;
	private boolean running = false;
	
	//OBJECTS
	private Tanks tanks;
	
	synchronized void start() {
		if (running) return;
		mainThread = new Thread(this);
		mainThread.start();
		
		running = true;
	}
	
	synchronized void stop() {
		if (!running) return;
		try {
			mainThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		running = false;
	}
	
	private void init() {
		tanks = new Tanks();
		addKeyListener(new KeyManager());
	}
	
	private void update() {
		tanks.update();
		WIDTH = (int)this.getSize().getWidth();
		HEIGHT = (int)this.getSize().getHeight();
	}
	
	private void draw() {
		BufferStrategy bs = this.getBufferStrategy();
		
		if (bs == null) {
			createBufferStrategy(3);
			return;
		}
		
		Graphics g = bs.getDrawGraphics();
		
		tanks.draw(g);

		g.dispose();
		bs.show();
	}
	
	public static void main(String[] args) {
		JFrame f = new JFrame(title);
		Engine e = new Engine();
		
		BufferedImage icon = new ImageLoader().load("/icon.png");
		
		f.setIconImage(icon);
		f.setSize(new Dimension(WIDTH, HEIGHT));
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setLocationRelativeTo(null);
//		f.setResizable(false);
		f.setVisible(true);
		
		f.add(e);
		e.start();
	}
	
	@Override
	public void run() {
		init();
		
		long lastTime = System.nanoTime();
		double amountOfTicks = 60D;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0D;
		
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			
			if (delta >= 1) {
				update();
				draw();
				
				delta--;
			}
		}
		
		stop();
	}

}
